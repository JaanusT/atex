package selenium;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import selenium.Models.User;
import selenium.pages.FormPage;
import selenium.pages.ListPage;
import selenium.pages.LoginPage;
import selenium.pages.MenuPage;

public class PageObjectTest {

    private static final String USERNAME = "user";
    private static final String WRONG_PASSWORD = "2";
    private static final String CORRECT_PASSWORD = "1";


    @Test
    public void loginFailsWithFalseGredentials() {
        LoginPage loginPage = LoginPage.goTo();
        loginPage.logInWithExpectingFailure(USERNAME, WRONG_PASSWORD);
        assertThat(loginPage.getErrorMessage(), is(notNullValue()));
    }

    @Test
    public void loginSucceedsWithCorrectCredentials() {
        LoginPage loginPage = LoginPage.goTo();
        MenuPage menu = loginPage.logInWithCorrectParameter(USERNAME, CORRECT_PASSWORD);
        assertThat(menu, is(notNullValue()));
    }

    @Test
    public void logOutDirectsToLoginPage() {
        LoginPage loginPage = LoginPage.goTo();
        MenuPage menu = loginPage.logInWithCorrectParameter(USERNAME, CORRECT_PASSWORD);
        assertThat(menu.logOutSucceeds(), is(true));

    }

    @Test
    public void formPageBtnDirectsToFormPage() {
        LoginPage loginPage = LoginPage.goTo();
        MenuPage menu = loginPage.logInWithCorrectParameter(USERNAME, CORRECT_PASSWORD);
        FormPage page = menu.formPageButton();
        assertThat(page, is(notNullValue()));
    }

    @Test
    public void listPageBtnDirectsToListPage(){
        LoginPage loginPage = LoginPage.goTo();
        MenuPage menu = loginPage.logInWithCorrectParameter(USERNAME, CORRECT_PASSWORD);
        ListPage page = menu.listPageButton();
        assertThat(page, is(notNullValue()));
    }

    @Test
    public void addNewUserDirectsToMenuPage() {
        LoginPage loginPage = LoginPage.goTo();
        MenuPage menu = loginPage.logInWithCorrectParameter(USERNAME, CORRECT_PASSWORD);
        FormPage page = menu.formPageButton();
        MenuPage mp = page.addNewUser("seltest", "seltest");
        assertThat(mp, is(notNullValue()));
    }

    @Test
    public void listPageContainsBasicUser(){
        LoginPage loginPage = LoginPage.goTo();
        MenuPage menu = loginPage.logInWithCorrectParameter(USERNAME, CORRECT_PASSWORD);
        ListPage listPage = menu.listPageButton();
        User basicUser = new User("user","1");
        assertThat(listPage.getListOfUsers().contains(basicUser),is(true));
    }

    @Test
    public void addedUserIsShownOnListPage(){
        LoginPage loginPage = LoginPage.goTo();
        MenuPage menu = loginPage.logInWithCorrectParameter(USERNAME, CORRECT_PASSWORD);
        FormPage formpage = menu.formPageButton();
        MenuPage mp = formpage.addNewUser("seltest","seltest");
        ListPage lp = mp.listPageButton();
        User newUser = new User("seltest","seltest");
        assertThat(lp.getListOfUsers().contains(newUser),is(true));
    }



}
