package selenium.Models;

/**
 * Created by JaanusTürnpuu on 03-Jan-17.
 */
public class User {
    String username;
    String password;

    public User(String username, String password) {

        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object object) {
        boolean result = false;

        if (object != null && object instanceof User) {
            if (((User) object).getPassword().matches(this.password) && ((User) object).getUsername().matches(this.getUsername())) {
                return true;
            }
        }

        return result;
    }


}
