package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MenuPage extends AbstractPage {

    public MenuPage(WebDriver driver) {
        super(driver);
        if (elementById("menu_page") == null) {
            throw new IllegalStateException("not on menu page");
        }
    }

    public Boolean logOutSucceeds() {
        elementById("log_out_link").click();
        WebElement element = elementById("login_page");
        if (element != null) {
            return true;
        } else {
            return false;
        }
    }

    public FormPage formPageButton() {
        elementById("add_user_link").click();
        WebElement element = elementById("form_page");
        if (element != null) {
            return new FormPage(this.driver);
        } else {
            throw new IllegalStateException("not on form page!");
        }
    }

    public ListPage listPageButton(){
        elementById("show_users_link").click();
        WebElement element = elementById("list_page");
        if (element != null) {
            return new ListPage(this.driver);
        } else {
            throw new IllegalStateException("not on list page!");
        }
    }
}
