package Rpn;
import org.junit.Test;

import java.util.Stack;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * Created by JaanusTürnpuu on 19-Sep-16.
 */
public class Rpn {
    @Test
    public void newCalculatorHasZeroInAccumulator(){
        Calc c = new Calc();
        assertThat(c.getAccumulator(),is(0));

    }

    @Test
    public void setAcumulatorTest(){
        Calc c = new Calc();
        c.setAccumulator(3);
        assertThat(c.getAccumulator(),is(3));


    }


    @Test
    public void calculatorSupportsAddition(){
        Calc c = new Calc();
        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        assertThat(c.getAccumulator(), is(3));
    }

    @Test
    public void calculatorSupportsMultiplication(){
        Calc c = new Calc();
        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.multiply();
        assertThat(c.getAccumulator(), is(2));

    }
    @Test
    public void calculateLongerExperssionTest(){
        Calc c = new Calc();
        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        c.enter();
        c.setAccumulator(4);
        c.multiply();
        assertThat(c.getAccumulator(),is(12));
    }
    @Test
    public void calculateLongerExpressionTest2(){
        Calc c = new Calc();
        c.setAccumulator(4);
        c.enter();
        c.setAccumulator(3);
        c.plus();
        c.enter();
        c.setAccumulator(2);
        c.enter();
        c.setAccumulator(1);
        c.plus();
        c.multiply();
        assertThat(c.getAccumulator(),is(21));

    }

    @Test
    public void calculatorEvaluateExpressionTest(){
        Calc c = new Calc();
       int result = c.evaluate("5 1 2 + 4 * + 3 +");
        assertThat(result,is(20));

    }
    @Test
    public void calculatorEvaluateExpressionTest2(){
        Calc c = new Calc();
        int result = c.evaluate("5 2 1 + * ");
        assertThat(result,is(15));

    }
    private class Calc {
        private int accumulator;
        private Stack<Integer> acmStack = new Stack<>();
        public Calc(){
            accumulator = 0;
        }
        public int getAccumulator() {
            return accumulator;
        }


        public void setAccumulator(int i) {
            accumulator = i;
        }

        public void enter() {
            acmStack.push(accumulator);
        }

        public void plus() {
            int result = acmStack.pop() + accumulator;
            accumulator = result;
        }

        public void multiply() {
            int result = acmStack.pop() * accumulator;
            accumulator = result;
        }

        public int evaluate(String s) {
            String [] nums = s.split("");
            for ( String x: nums) {
               if(tryParseInt(x)){
                   acmStack.push(Integer.parseInt(x));
               } else {
                   if(x.equals("+")){
                    setAccumulator(acmStack.pop());
                    plus();
                       enter();

                   } else if(x.equals("*")){
                       setAccumulator(acmStack.pop());
                       multiply();
                       enter();
                   }

               }

            }
            return accumulator;
        }

        private boolean tryParseInt(String num) {
            try {
                Integer.parseInt(num);
                return true;
            }catch (NumberFormatException e) {
                return false;
            }

        }
    }
}
