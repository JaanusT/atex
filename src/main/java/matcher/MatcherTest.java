package matcher;

import org.hamcrest.CoreMatchers.*;
import org.hamcrest.Matcher;
import org.hamcrest.core.Is;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;


/**
 * Created by JaanusTürnpuu on 26-Dec-16.
 */
public class MatcherTest {

    private IsGreaterThanMatcher isGreaterThan(int value) {
        return new IsGreaterThanMatcher(value);
    }

    @Test
    public void testTwoIsGreaterThanOne(){
        assertThat(2, is(isGreaterThan(1)));

    }

    @Test
    public void testOneIsNotGreaterThanTwo(){
        assertThat(1, is(not(isGreaterThan(2))));
    }




    public static <T> void assertThat(
            int actual, Matcher<? super T> matcher) {
        if (!matcher.matches(actual))
            throw new AssertionError("does not match");
    }

}
