package matcher;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class IsGreaterThanMatcher
              extends TypeSafeMatcher<Integer> {
    
    private Integer expected;

    public IsGreaterThanMatcher(Integer expected) {
      this.expected = expected;
    }
    
    @Override
    protected boolean matchesSafely(Integer actual) {
      return actual > expected;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("Greater than " + expected);
    }
}