package gameoflife;

class Frame {

    private final static String ALIVE = "X";
    private final static String DEAD = "-";
    private int xSize;
    private int ySize;
    private String[][] frame;

    Frame(int width, int height) {
        xSize = width;
        ySize = height;
        this.frame = new String[width][height];
        fillFrameWithDeadCells();
    }


    Frame(String[] data) {
        xSize = data.length;
        ySize = getYSize(data);
        this.frame = new String[xSize][ySize];
        createGameFrame(data);
    }

    private int getYSize(String[] data) {
        return data[0].length();
    }

    private void fillFrameWithDeadCells() {
        for (int i = 0; i < xSize; i++) {
            for (int j = 0; j < ySize; j++) {
                markDead(i, j);
            }
        }
    }

    private void createGameFrame(String[] rows) {
        for (int i = 0; i < xSize; i++) {
            String[] cells = rows[i].split("");
            for (int j = 0; j < cells.length; j++) {
                if ("-".equalsIgnoreCase(cells[j])) {
                    markDead(i, j);
                } else {
                    markAlive(i, j);
                }
            }
        }
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < xSize; i++) {
            for (int j = 0; j < ySize; j++) {
                if (isAlive(i, j))
                    sb.append('X');
                else
                    sb.append('-');
            }
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (getClass() != obj.getClass())
            return false;
        Frame newFrame = (Frame) obj;
        return (this.toString().equals(newFrame.toString()));

    }

    Integer getNeighboursCount(int x, int y) {
        int count = 0;
        for (int i = x - 1; i <= x + 1; i++) {
            if (i >= 0 && i < xSize)
                for (int j = y - 1; j <= y + 1; j++) {
                    if (j >= 0 && j < ySize)
                        if (i != x || j != y)
                            if (isAlive(i, j)) {
                                count++;
                            }
                }
        }
        return count;
    }


    boolean isAlive(int x, int y) {
        return frame[x][y].matches(ALIVE);
    }

    void markAlive(int x, int y) {
        this.frame[x][y] = ALIVE;
    }

    void markDead(int x, int y) {
        this.frame[x][y] = DEAD;
    }

    Frame nextFrame() {
        Frame newFrame = cloneFrame();
        for (int i = 0; i < xSize; i++)
            for (int j = 0; j < ySize; j++) {
                int nCount = getNeighboursCount(i, j);
                if (nCount < 2) {
                    newFrame.markDead(i, j);
                }
                if (nCount > 3) {
                    newFrame.markDead(i, j);
                }
                if (nCount == 3) {
                    newFrame.markAlive(i, j);
                }
            }

        return newFrame;
    }


    String[] getDataFromFrame(Frame frame) {
        return frame.toString().split(System.getProperty("line.separator"));
    }

    Frame cloneFrame() {
        return new Frame(getDataFromFrame(this));
    }
}
