package gameoflife;


import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class GameOfLifeTest {


    @Test
    public void markGivenCellAlive() {
        Frame frame = new Frame(5, 5);
        frame.markAlive(1, 1);
        assertThat(frame.isAlive(1, 1), is(true));

    }

    @Test
    public void markGivenCellDead(){
        Frame frame = new Frame(5, 5);
        frame.markAlive(1, 1);
        assertThat(frame.isAlive(1, 1), is(true));
        frame.markDead(1,1);
        assertThat(frame.isAlive(1, 1), is(false));
    }

    @Test
    public void createFrameAndTestGivenCellIsAlive() {
        Frame frame = getFrame("----",
                "-X--",
                "--X-",
                "----");
        assertThat(frame.isAlive(1, 1), is(true));

    }

    @Test
    public void cellHas2Neighbors() {
        Frame frame = getFrame("------",
                "-XX---",
                "-X----",
                "----X-",
                "---XX-",
                "------");
        assertThat(frame.getNeighboursCount(1, 1), is(2));

    }

    @Test
    public void countCornerCellNeighbors() {
        Frame frame = getFrame("------",
                "-XX---",
                "-X----",
                "----X-",
                "---XX-",
                "------");
        assertThat(frame.getNeighboursCount(1, 6), is(0));

    }

    @Test
    public void cellSurroundedByNeighbors() {
        Frame frame = getFrame(
                "XXX---",
                "XXX---",
                "XXX---",
                "----X-",
                "---XX-",
                "------");
        assertThat(frame.getNeighboursCount(1, 1), is(8));

    }

    @Test
    public void cellHas4Neighbors() {
        Frame frame = getFrame("------",
                "-XX---",
                "-XX---",
                "---XX-",
                "---XX-",
                "------");
        assertThat(frame.getNeighboursCount(3, 3), is(4));
    }

    @Test
    public void printOutFrameToTestToString() {
        Frame frame = getFrame("----",
                "-X--",
                "--X-",
                "----");
        System.out.println(frame);
    }

    @Test
    public void cloneAndPrintFrames() {
        Frame frame = getFrame("----",
                "-X--",
                "--X-",
                "----");
        Frame clone = frame.cloneFrame();
        System.out.print(frame);
        System.out.println("_____________");
        System.out.print(clone);
    }

    @Test
    public void cloneAndCompareFramesUsingEquals() {
        Frame frame = getFrame("----",
                "-X--",
                "--X-",
                "----");
        Frame clone = frame.cloneFrame();
        assertThat(frame.equals(clone), is(true));
    }

    @Test
    public void allCellsDieTest() {
        Frame frame = getFrame("----",
                "-X--",
                "--X-",
                "----");
        Frame expected = getFrame("----",
                "----",
                "----",
                "----");
        assertThat(frame.nextFrame(), is(equalTo(expected)));

    }

    @Test
    public void stillWorksCorrectly() {
        Frame frame = getFrame("------",
                "--XX--",
                "-X--X-",
                "--XX--",
                "------");


        assertThat(frame.nextFrame(), is(equalTo(frame)));
    }

    @Test
    public void cloneAndCompareFramesUsingEquals2() {
        Frame frame = getFrame("------",
                "-XX---",
                "-XX---",
                "---XX-",
                "---XX-",
                "------");
        Frame clone = frame.cloneFrame();
        assertThat(frame.equals(clone), is(true));
    }

    @Test
    public void pulsarWorksCorrectly() {
        Frame frame = getFrame("------",
                "-XX---",
                "-X----",
                "----X-",
                "---XX-",
                "------");

        Frame expected = getFrame("------",
                "-XX---",
                "-XX---",
                "---XX-",
                "---XX-",
                "------");

        assertThat(frame.nextFrame(), is(equalTo(expected)));
        assertThat(frame.nextFrame().nextFrame(), is(equalTo(frame)));
    }

    @Test
    public void gliderWorksCorrectly() {
        Frame frame1 = getFrame("-X----",
                "--XX--",
                "-XX---",
                "------");

        Frame frame2 = getFrame("--X---",
                "---X--",
                "-XXX--",
                "------");

        Frame frame3 = getFrame("------",
                "-X-X--",
                "--XX--",
                "--X---");

        assertThat(frame1.nextFrame(), is(equalTo(frame2)));
        assertThat(frame2.nextFrame(), is(equalTo(frame3)));
    }

    @Test
    public void moreComplexTransormationTest() {
        Frame frame = getFrame("-X----",
                "--XX--",
                "-XX---",
                "------");
        String[] data = frame.getDataFromFrame(frame);
        Frame frameNew = getFrame(data);
        assertThat(frame.nextFrame(), is(equalTo(frameNew.nextFrame())));
    }

    private Frame getFrame(String... rows) {
        return new Frame(rows);
    }

}