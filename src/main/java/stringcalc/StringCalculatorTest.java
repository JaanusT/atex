package stringcalc;

import org.junit.Test;
import org.omg.PortableInterceptor.Interceptor;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class StringCalculatorTest {

    // "" -> 0
    // "1" -> 1
    // "1, 2" -> 3
    // null -> IllegalArgumentException
    @Test
    public void EmptyInput_IsZero(){
        StringCalculator calc = new StringCalculator();
      int result =  calc.add("");
        assertThat(result,is(0));
        

    }
    @Test
    public void OneInput_SameValue(){
        StringCalculator calc = new StringCalculator();
        int result =  calc.add("1");
        assertThat(result,is(1));
    }
    @Test
    public void Add2Elements(){
        StringCalculator calc = new StringCalculator();
        int result =  calc.add("1,2");
        assertThat(result,is(3));
    }
    @Test(expected = IllegalArgumentException.class)
    public void NullInput_IllegalArgsExcepted(){
        StringCalculator calc = new StringCalculator();
        calc.add(null);

    }

  class StringCalculator{

      public int add(String s) {
          if(s==null){
              throw new IllegalArgumentException();
          }
          if(s.equals("")){
              return 0;
          }

          if(s.length()==1){
              return Integer.parseInt(s);
          }
          String [] nums = s.split(", ?");
          int sum = 0;
          for (int i = 0; i < nums.length; i++) {
              sum+=Integer.parseInt(nums[i]);
          }

          return sum;
      }
  }


}



