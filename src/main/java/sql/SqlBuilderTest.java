package sql;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SqlBuilderTest {

    @Test
    public void testSelectFrom() {
        SqlBuilder b = new SqlBuilder();
        b.selectColumn("a");
        b.from("t");

        assertThat(b.getSql(), is("select a from t"));
    }

    @Test
    public void testSelectMultipleColumns() {
        SqlBuilder b = new SqlBuilder();
        b.selectColumns("a", "b");
        b.from("t");

        assertThat(b.getSql(), is("select a, b from t"));
    }

    @Test
    public void testSelectWithWhereCondition() {
        SqlBuilder b = new SqlBuilder();
        b.selectColumn("a");
        b.from("t");
        b.where("id = ?", 1);

        assertThat(b.getSql(), is("select a from t where id = ?"));
        assertThat(b.getParameters(), is(Arrays.asList(1)));

    }

    @Test
    public void testMultipleWhereConditions() {
        SqlBuilder b = new SqlBuilder();
        b.selectColumn("a");
        b.from("t");
        b.where("is_hidden = 1");
        b.where("deleted_on is null");

        assertThat(b.getSql(),
                is("select a from t where is_hidden = 1 and deleted_on is null"));
    }

    @Test
    public void testWhereConditionsNotNull() {
        SqlBuilder b = new SqlBuilder();
        b.selectColumn("a");
        b.from("t");
        b.eqIfNotNull("a", 1);
        b.eqIfNotNull("b", null);
        b.eqIfNotNull("c", 3);

        assertThat(b.getSql(), is("select a from t where a = ? and c = ?"));
        assertThat(b.getParameters(), is(Arrays.asList(1, 3)));
    }

    @Test
    public void testInCondition() {
        SqlBuilder b = new SqlBuilder();
        b.selectColumn("a");
        b.from("t");
        b.in("id", Arrays.asList(1, 2));

        assertThat(b.getSql(), is("select a from t where id in (?, ?)"));
        assertThat(b.getParameters(), is(Arrays.asList(1, 2)));
    }

    @Test
    public void testLeftJoin() {
        SqlBuilder b = new SqlBuilder();
        b.selectColumn("a");
        b.from("t");
        b.leftJoin("u", "u.id = t.u_id");

        assertThat(b.getSql(), is("select a from t left join u on u.id = t.u_id"));
    }

    @Test
    public void testFromNewSqlBulder() {
        SqlBuilder sub = new SqlBuilder();
        sub.selectColumn("b");
        sub.from("t");

        SqlBuilder b = new SqlBuilder();
        b.selectColumn("a");
        b.from(sub);

        assertThat(b.getSql(), is("select a from (select b from t)"));
    }
}
