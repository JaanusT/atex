package stub;

import common.Money;

public class TestableBank extends Bank {


    public Money convert(Money money, String toCurrency) {
        if ("EEK".equals(money.getCurrency()) && "EUR".equals(toCurrency)) {
            return new Money(money.getAmount() / 15, "EUR");
        } else if ("EUR".equals(money.getCurrency()) && "EEK".equals(toCurrency)) {
            return new Money(money.getAmount() * 15, "EEK");
        } else
        return money;
    }
}
