package invoice;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class InvoiceRowGenerator {

    private InvoiceRowDao invoiceRowDao;

    public void generateRowsFor(BigDecimal amount, Date start, Date end) {
        if (amount.intValue() <= 0) {
            throw new IllegalArgumentException("Payment amount cant be zero or smaller!");
        }
        List<Date> paymentDates = getPaymentDates(start, end);
        List<BigDecimal> paymentSizes = getMonthPayments(amount, paymentDates.size());
        int m = paymentSizes.size() - 1;
        for (int i = 0; i < paymentDates.size() && m >= 0; i++) {
            InvoiceRow ir = new InvoiceRow(paymentSizes.get(m), paymentDates.get(i));
            invoiceRowDao.save(ir);
            m--;
        }

    }


    private List<Date> getPaymentDates(Date start, Date end) {
        if (end.before(start)) {
            throw new IllegalArgumentException("End date cannot be before start date!");
        }
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(start);

        Calendar endCalnedar = Calendar.getInstance();
        endCalnedar.setTime(end);
        endCalnedar.set(Calendar.DATE, endCalnedar.getActualMinimum(Calendar.DAY_OF_MONTH));

        List<Date> dates = new ArrayList<>();
        dates.add(start);

        while (startCalendar.getTime().before(endCalnedar.getTime())) {
            startCalendar.add(Calendar.MONTH, 1);
            startCalendar.set(Calendar.DATE, startCalendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            dates.add(startCalendar.getTime());
        }

        return dates;
    }

    private List<BigDecimal> getMonthPayments(BigDecimal amount, int monthsToPay) {
        List<BigDecimal> payments = new ArrayList<>();
        if (amount.intValue() < 3) {
            payments.add(amount);
            return payments;
        }
        BigDecimal amountLeft = amount;
        for (int i = 0; i < monthsToPay; ) {
            if (amount.intValue() / monthsToPay < 3) {
                payments.add(amount.subtract(new BigDecimal(String.valueOf(3))));
                payments.add(new BigDecimal(String.valueOf(3)));
                return payments;
            }
            BigDecimal payment = amountLeft.divide(new BigDecimal(String.valueOf(monthsToPay)), RoundingMode.UP);
            BigDecimal divider = amountLeft.divide(new BigDecimal(String.valueOf(3)), BigDecimal.ROUND_HALF_UP);
            if (divider.doubleValue() < 1) {
                payments.add(BigDecimal.valueOf(3));
                return payments;
            }

            payments.add(payment);
            amountLeft = amountLeft.subtract(payment);
            monthsToPay--;

        }
        return payments;
    }
}

