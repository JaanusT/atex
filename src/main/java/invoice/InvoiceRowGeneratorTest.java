package invoice;

import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.text.*;
import java.util.Date;
import org.hamcrest.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceRowGeneratorTest {

    @Mock
    InvoiceRowDao dao;

    @InjectMocks
    InvoiceRowGenerator generator;

    public static Date asDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void calculatesCorrectPaymentDates() {
        generator.generateRowsFor(new BigDecimal(6), asDate("2012-01-01"), asDate("2012-02-02"));
        verify(dao, times(2)).save(argThat(getMatcherForAmount(3)));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-01-01"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-02-01"))));
        Mockito.verifyNoMoreInteractions(dao);
    }


    @Test
    public void dividesEquallyWhenNoRemainder() {

        generator.generateRowsFor(new BigDecimal(9),
                asDate("2012-02-15"),
                asDate("2012-04-02"));

        verify(dao, times(3)).save(argThat(getMatcherForAmount(3)));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-02-15"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-03-01"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-04-01"))));
        Mockito.verifyNoMoreInteractions(dao);
    }

    @Test
    public void dividesCorrectlyWithRemainder() {
        generator.generateRowsFor(new BigDecimal(11),
                asDate("2012-01-02"),
                asDate("2012-03-01"));

        verify(dao, times(1)).save(argThat(getMatcherForAmount(3)));
        verify(dao, times(2)).save(argThat(getMatcherForAmount(4)));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-01-02"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-02-01"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-03-01"))));
        Mockito.verifyNoMoreInteractions(dao);

    }

    @Test
    public void putsTogetherPaymentsSmallerThanThreeEuros() {
        generator.generateRowsFor(new BigDecimal(7),
                asDate("2012-02-02"),
                asDate("2012-05-02"));
        verify(dao, times(1)).save(argThat(getMatcherForAmount(3)));
        verify(dao, times(1)).save(argThat(getMatcherForAmount(4)));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-02-02"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-03-01"))));
        Mockito.verifyNoMoreInteractions(dao);
    }

    @Test
    public void amountUnderThreeEurosGetsOnlyOnePayment() {
        generator.generateRowsFor(new BigDecimal(2),
                asDate("2012-02-02"),
                asDate("2012-05-02"));
        verify(dao, times(1)).save(argThat(getMatcherForAmount(2)));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-02-02"))));
        Mockito.verifyNoMoreInteractions(dao);
    }


    @Test(expected = IllegalArgumentException.class)
    public void amountZeroOrUnderThrowsException() {
        generator.generateRowsFor(new BigDecimal(0),
                asDate("2012-02-02"),
                asDate("2012-05-02"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void endDateBeforeStartThrowsException() {
        generator.generateRowsFor(new BigDecimal(0),
                asDate("2013-02-02"),
                asDate("2012-05-02"));
    }

    private Matcher<InvoiceRow> getMatcherForAmount(final Integer amount) {

        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return amount.equals(item.amount.intValue());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(String.valueOf(amount));
            }
        };
    }

    private Matcher<InvoiceRow> getMatcherForDate(final Date date) {


        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return date.equals(item.date);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(String.valueOf(date));
            }
        };
    }

}