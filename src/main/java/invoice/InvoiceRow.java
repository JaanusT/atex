package invoice;

import java.math.BigDecimal;
import java.util.Date;

class InvoiceRow {

    public BigDecimal getAmount() {
        return amount;
    }

    public final BigDecimal amount;

    public Date getDate() {
        return date;
    }

    public final Date date;

    public InvoiceRow(BigDecimal amount, Date date) {
        this.amount = amount;
        this.date = date;
    }
}