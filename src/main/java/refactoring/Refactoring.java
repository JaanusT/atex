package refactoring;

import java.util.*;

public class Refactoring {

    // 1a
    public int incrementInvoiceNumber() {
        return ++invoiceNumber;
    }

    // 1b
    public void addFilledOrdersToList(List<Order> orderList) {
        for (Order order : orders) {
            if (order.isFilled()) {
                orderList.add(order);
            }
        }
    }

    // 2a
    public void printStatementFor(Long invoiceId) {
        List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);
        printInvoiceRows(invoiceRows);
        double total = getTotal(invoiceRows);
        printValue(total);
    }

    private double getTotal(List<InvoiceRow> invoiceRows) {
        double total = 0;
        for (InvoiceRow invoiceRow : invoiceRows) {
            total += invoiceRow.getAmount();
        }
        return total;
    }

    // 2b
    public String getItemsAsHtml() {
        String retval = "";
        for (String item : Arrays.asList(item1, item2, item3, item4)) {
            retval += encloseLiTag(item,"li");
        }
        return encloseLiTag(retval,"ul");
    }
    private String encloseLiTag(String tagitem, String tag) {
        if(tag=="ul"){
            StringBuilder sb = new StringBuilder(tagitem);
            sb.insert(0, "<ul>");
            sb.append("</ul>");
            return sb.toString();
        }
        return "<li>" + tagitem + "</li>";
    }


    // 3
    public boolean isSmallOrder() {
        return (order.getTotal() > 100);
    }

    // 4
    public void printPrice() {
        System.out.println("Hind ilma käibemaksuta: " + getBasePrice());
        System.out.println("Hind käibemaksuga: " + getBasePrice() * turnOverRate);
    }

    // 5
    public void calculatePayFor(Job job) {
        if (holidayNight(job)) {

        }
    }

    private boolean holidayNight(Job job) {
        return (job.day == 6 || job.day == 7)
                && (job.hour > 20 || job.hour < 7);
    }

    // 6
    public boolean canAccessResource(SessionData sessionData) {
        return hasRequiredPrivileges(sessionData);
    }

    private boolean hasRequiredPrivileges(SessionData sessionData) {
        return (sessionData.getCurrentUserName().equals("Admin")
                || sessionData.getCurrentUserName().equals("Administrator"))
                && (sessionData.getStatus().equals("preferredStatusX")
                || sessionData.getStatus().equals("preferredStatusY"));
    }

    // 7
    public void drawLines() {
        Space space = new Space();
        space.drawLine(new StartPoint(12, 3, 5), new EndPoint(2, 4, 6));
        space.drawLine(new StartPoint(2, 4, 6), new EndPoint(0, 1, 0));
    }

    // 8
    public int calculateWeeklyPay(int hoursWorked, boolean overtime) {
        int straightTime = Math.min(40, hoursWorked);
        int overTime = Math.max(0, hoursWorked - straightTime);
        int straightPay = straightTime * hourRate;
        if(overtime)
            return calculateWeeklyPayWithOvertime(overTime,straightPay);
        else
        return straightPay;
    }

    private int calculateWeeklyPayWithOvertime(int overTime,int straightPay) {
        double overtimeRate =  1.5 * hourRate;
        return (int) Math.round(overTime * overtimeRate) + straightPay;
    }

    // //////////////////////////////////////////////////////////////////////////

    // Abiväljad ja abimeetodid.
    // Need on siin lihtsalt selleks, et kood kompileeruks

    private String item1 = "1";
    private String item2 = "2";
    private String item3 = "3";
    private String item4 = "4";
    private int hourRate = 5;
    int invoiceNumber = 0;
    private List<Order> orders = new ArrayList<Order>();
    private Order order = new Order();
    private Dao dao = new SampleDao();
    private double price = 0;
    private double turnOverRate = 1.2;

    void justACaller() {
        incrementInvoiceNumber();
        addFilledOrdersToList(null);
    }

    private void printValue(double total) {
    }

    private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
    }

    class Space {
        public void drawLine(StartPoint startPoint, EndPoint endPoint) {
        }

    }

    interface Dao {
        List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
    }

    class SampleDao implements Dao {
        @Override
        public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
            return null;
        }
    }

    class Order {
        public boolean isFilled() {
            return false;
        }

        public double getTotal() {
            return 0;
        }
    }

    class InvoiceRow {
        public double getAmount() {
            return 0;
        }
    }

    class Job {
        public int hour;
        public int day;
    }

    private double getBasePrice() {
        return price;
    }

    private class SessionData {

        public String getCurrentUserName() {
            return null;
        }

        public String getStatus() {
            return null;
        }

    }

}
