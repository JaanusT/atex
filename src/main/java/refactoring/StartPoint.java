package refactoring;

public class StartPoint {
    private final int x1;
    private final int y1;
    private final int z1;

    public StartPoint(int x1, int y1, int z1) {
        this.x1 = x1;
        this.y1 = y1;
        this.z1 = z1;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getZ1() {
        return z1;
    }
}
