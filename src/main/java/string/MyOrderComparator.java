package string;
import java.util.Date;
import java.util.Comparator;

/**
 * Created by JaanusTürnpuu on 26-Dec-16.
 */
public class MyOrderComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        Date date1 = ((Order) o1).getOrderDate();
        Date date2 = ((Order) o2).getOrderDate();
        return date1.compareTo(date2);
    }
}
