package string;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class OrderService {

    private DataSource dataSource;

    public OrderService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> getFilledOrders()
    {
        List<Order> result = new ArrayList<>();
        for(Order o : dataSource.getOrders()){
            if(o.isFilled())
                result.add(o);
        }
        return result;
    }

    public List<Order> getOrdersOver(double amount)
    {
        List<Order> result = new ArrayList<>();
        for(Order o : dataSource.getOrders()){
            if(o.getTotal() > amount)
                result.add(o);
        }
        return result;
    }

    public List<Order> getOrdersSortedByDate()
    {
        List<Order> result = new ArrayList<>();
        for(Order o : dataSource.getOrders()){
            result.add(o);
        }
        java.util.Collections.sort(result, new MyOrderComparator());
        return result;
    }
}
