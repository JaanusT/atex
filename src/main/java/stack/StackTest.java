package stack;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@SuppressWarnings("unused")
public class StackTest {

    @Test
    public void newStackHasNoElements() {
        Stack stack = new Stack(100);

        assertThat(stack.getSize(), is(0));
    }
    //- loo pinu, lisa (push) kaks elementi ja kontrolli, et selles on 2 elementi (size == 2).

    @Test
    public void newStackHas2Elements() {
        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(2);

        assertThat(stack.getSize(), is(2));
    }

    //- loo pinu, lisa (push) kaks elementi, vُta (pop) kaks elementi ja kontrolli, et   pinus on 0 elementi.


    @Test
    public void newStackAdd2Pop2() {
        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(2);
        stack.pop();
        stack.pop();
        assertThat(stack.getSize(), is(0));
    }

    /*- loo pinu, lisa (push) kaks elementi, vُta (pop) kaks elementi ja kontrolli, et
    need on needsamad lisatud elemendid.*/

    @Test
    public void newStackAdd2Pop2_CheckElements() {
        Stack stack = new Stack(100);
        stack.push(1);
        assertThat(stack.pop(), is(1));
        stack.push(2);
        assertThat(stack.pop(), is(2));
        assertThat(stack.getSize(), is(0));
    }

    /*- loo pinu, lisa (push) kaks elementi, vaata pealmist elementi (peek) ja kontrolli,
  et see on ُige element.*/


    @Test
    public void newStackPush2PeekLast() {
        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(2);
        assertThat(stack.peek(), is(2));
    }

    /*- loo pinu, lisa (push) kaks elementi, vaata pealmist elementi ja kontrolli, et
  pinus on 2 elementi.*/

    @Test
    public void newStackPush2PeekLastCheckSize() {
        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(2);
        assertThat(stack.peek(), is(2));
        assertThat(stack.getSize(), is(2));
    }

    /*- loo pinu, lisa (push) kaks elementi, vaata pealmist elementi, vaata uuesti pealmist
  elementi ja kontrolli, et see on seesama, mis esimesel korral.*/

    @Test
    public void newStackPush2PeekLast2Times() {
        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(2);
        assertThat(stack.peek(), is(2));
        assertThat(stack.peek(), is(2));
    }

    /*- loo pinu, vُta (pop) üks element ja kontrolli, et pinu viskab IllegalStateException-i.
*/

    @Test(expected = IllegalStateException.class)
    public void newEmptyStackPop() {
        Stack stack = new Stack(100);
        stack.pop();
    }

    /*- loo pinu, vaata pealmist elementi ja kontrolli, et pinu viskab IllegalStateException-i.
*/
    @Test(expected = IllegalStateException.class)
    public void newEmptyStackPeekLast(){
        Stack stack = new Stack(100);
        stack.peek();
    }


}



class Stack {

    private Integer[] _stack;
    private int _lastIndex =-1;


    public Stack(int i) {
    _stack = new Integer[100];
    }

    public int  getSize() {
        if(_stack[0]!=null){
           return _lastIndex+1;
        }
        return 0;
    }

    public void push(int i) {
        _lastIndex++;
        _stack[_lastIndex]=i;
    }

    public int pop() {
        if(_stack[0]==null){
            throw new IllegalStateException();
        }
        int last = _stack[_lastIndex];
        _stack[_lastIndex]=null;
        _lastIndex--;
        return last;
    }

    public int  peek() {
        if(_stack[0]==null){
            throw new IllegalStateException();
        }
        return _stack[_lastIndex];
    }
}